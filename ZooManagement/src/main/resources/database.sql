create database zoo_management;
use zoo_management;

create table AnimalCategory
(
id int NOT NULL AUTO_INCREMENT,
name varchar(20),
description varchar(255),
validity tinyint DEFAULT 1 NOT NULL,
PRIMARY KEY (id)
);

create table Enclosure 
(
id int NOT NULL AUTO_INCREMENT,
capacity int,
type varchar(20),
validity tinyint DEFAULT 1 NOT NULL,
PRIMARY KEY (id)
);

create table Role
(
id int NOT NULL AUTO_INCREMENT,
title varchar(20),
validity tinyint DEFAULT 1 NOT NULL,
PRIMARY KEY (id)
);

create table Permission
(
id int NOT NULL AUTO_INCREMENT,
name varchar(20),
validity tinyint DEFAULT 1 NOT NULL,
PRIMARY KEY(id)
);

create table Role_Permission
(
roleId int,
permissionId int,
PRIMARY KEY(roleId,permissionId),
FOREIGN KEY (roleId) REFERENCES Role(id),
FOREIGN KEY(permissionId) REFERENCES Permission(id)
);

create table Staff
(
id int NOT NULL AUTO_INCREMENT,
firstname varchar(20),
lastname varchar(20),
username varchar(20),
password varchar(255),
hiredate date,
reportsTo int,
roleId int,
validity tinyint DEFAULT 1 NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (roleId) REFERENCES Role(id),
FOREIGN KEY (reportsTo) REFERENCES Staff(id)
);

create table Staff_Enclosure
(
staffId int,
enclosureId int,
PRIMARY KEY (staffId,enclosureId),
FOREIGN KEY (enclosureId) REFERENCES Enclosure(id),
FOREIGN KEY (staffId) REFERENCES Staff(id)
);

create table Task
(
id int NOT NULL AUTO_INCREMENT,
name varchar(30),
description varchar(30),
createdBy int,
createdDate date,
status tinyint DEFAULT 0 NOT NULL,
staffId int,
validity tinyint DEFAULT 1 NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (staffId) REFERENCES Staff(id)
);

create table Report
(
id int NOT NULL AUTO_INCREMENT,
medicalObervation varchar(50),
dietObservation varchar(50),
comments varchar(50),
createdDate date,
taskId int,
validity tinyint DEFAULT 1 NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY(taskId) REFERENCES Task(id)
);

create table DailyDiet
(
id int NOT NULL AUTO_INCREMENT,
FirstSizePortion int,
LastSizePortion int,
unitMass varchar(10),
startDate date,
endDate date,
createdBy int,
createdDate date,
foodType varchar(20),
validity tinyint DEFAULT 1 NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY(createdBy) REFERENCES Staff(id)
);


create table Animal
(
id int NOT NULL AUTO_INCREMENT,
name varchar(20),
scientificName varchar(20),
age int,
weight double,
gender varchar(6),
createdDate date,
arrivalDate date,
enclosureId int,
dietId int,
categoryId int,
validity tinyint DEFAULT 1 NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY(enclosureId) REFERENCES Enclosure(id),
FOREIGN KEY (dietId) REFERENCES DailyDiet(id),
FOREIGN KEY (categoryId) REFERENCES AnimalCategory(id)
);
