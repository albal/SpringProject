package com.ikubInfo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikubInfo.daoImpl.AnimalCategoryDAOImpl;
import com.ikubInfo.pojo.AnimalCategory;
import com.ikubInfo.service.AnimalCategoryService;

@Service
public class AnimalCategoryServiceImpl implements AnimalCategoryService {

	@Autowired
	private AnimalCategoryDAOImpl animalCategoryDAOImpl;

	public List<AnimalCategory> getAll() {
		return animalCategoryDAOImpl.getAll();
	}

	public AnimalCategory getById(Integer id) {
		return animalCategoryDAOImpl.getById(id);
	}
}
