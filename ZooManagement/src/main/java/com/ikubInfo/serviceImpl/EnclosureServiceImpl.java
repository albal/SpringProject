package com.ikubInfo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikubInfo.daoImpl.EnclosureDAOImpl;
import com.ikubInfo.pojo.Enclosure;
import com.ikubInfo.service.EnclosureService;

@Service
public class EnclosureServiceImpl implements EnclosureService {

	@Autowired
	private EnclosureDAOImpl enclosureDAOImpl;

	public List<Enclosure> getAll() {
		return enclosureDAOImpl.getAll();
	}

	@Override
	public Enclosure getById(int id) {
		return enclosureDAOImpl.getById(id);
	}

}
