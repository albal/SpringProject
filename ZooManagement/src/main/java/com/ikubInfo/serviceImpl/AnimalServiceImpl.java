package com.ikubInfo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikubInfo.daoImpl.AnimalDAOImpl;
import com.ikubInfo.pojo.Animal;
import com.ikubInfo.service.AnimalService;

@Service
public class AnimalServiceImpl implements AnimalService {

	@Autowired
	private AnimalDAOImpl animalDAOImpl;

	public List<Animal> getAll() {
		return animalDAOImpl.getAll();
	}

	public void save(Animal animal) {
		animalDAOImpl.save(animal);
	}

	public void update(Animal animal) {
		animalDAOImpl.update(animal);
	}

	public void delete(Animal animal) {
		animalDAOImpl.delete(animal);
	}

	public Animal getById(Integer id) {
		return animalDAOImpl.getById(id);
	}

	@Override
	public boolean existScientificName(String name) {
		return animalDAOImpl.existScientificName(name);
	}

	@Override
	public Animal getByName(String name) {
		return animalDAOImpl.getByName(name);
	}

	@Override
	public Long animalsInEnclosure(String name) {
		return animalDAOImpl.animalsInEnclosure(name);
	}

}
