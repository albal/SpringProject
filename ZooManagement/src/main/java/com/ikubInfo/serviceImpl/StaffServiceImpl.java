package com.ikubInfo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikubInfo.daoImpl.StaffDAOImpl;
import com.ikubInfo.pojo.Staff;
import com.ikubInfo.pojo.StaffTask;
import com.ikubInfo.service.StaffService;
import com.ikubInfo.util.Roles;

@Service
public class StaffServiceImpl implements StaffService {

	@Autowired
	private StaffDAOImpl staffDAOImpl;

	public List<Staff> getAll() {
		return staffDAOImpl.getAll();
	}

	public void save(Staff staff) {	
		staffDAOImpl.save(staff);
	}

	public void update(Staff staff) {
		staffDAOImpl.update(staff);
	}

	@Override
	public void delete(Staff staff) {
		staffDAOImpl.update(staff);
	}

	public Staff getById(Integer id) {
		return staffDAOImpl.getById(id);
	}

	public Staff login(String username, String password) {
		return staffDAOImpl.checklogin(username, password);
	}

	@Override
	public boolean existUsername(String username) {
		return staffDAOImpl.existUsername(username);
	}

	@Override
	public Staff getbyUsername(String username) {
		return staffDAOImpl.getbyUsername(username);
	}

	@Override
	public List<Staff> getStaffByRole() {
		String title = Roles.ZOOKEEPER.getName();
		return staffDAOImpl.getStaffByRole(title);
	}

	public void saveStaffInTask(StaffTask staffTask) {
		staffDAOImpl.saveStaffInTask(staffTask);
	}

}
