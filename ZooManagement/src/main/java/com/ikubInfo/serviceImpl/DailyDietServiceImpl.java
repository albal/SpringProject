package com.ikubInfo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikubInfo.daoImpl.DailyDietDAOImpl;
import com.ikubInfo.pojo.DailyDiet;
import com.ikubInfo.service.DailyDietService;

@Service
public class DailyDietServiceImpl implements DailyDietService {

	@Autowired
	DailyDietDAOImpl dailyDietDAOImpl;

	@Override
	public List<DailyDiet> getAll() {
		return dailyDietDAOImpl.getAll();
	}

	@Override
	public void save(DailyDiet dailyDiet) {
		dailyDietDAOImpl.save(dailyDiet);

	}

	@Override
	public void update(DailyDiet dailyDiet) {
		dailyDietDAOImpl.update(dailyDiet);

	}

	@Override
	public void delete(DailyDiet dailyDiet) {
		dailyDietDAOImpl.delete(dailyDiet);
	}
}
