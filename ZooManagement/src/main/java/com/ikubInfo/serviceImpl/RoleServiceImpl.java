package com.ikubInfo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikubInfo.daoImpl.RoleDAOImpl;
import com.ikubInfo.pojo.Role;
import com.ikubInfo.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDAOImpl roleDAOImpl;

	public List<Role> getAll() {
		return roleDAOImpl.getAll();
	}

	public void save(Role role) {
		roleDAOImpl.save(role);
	}

	@Override
	public void update(Role role) {
		roleDAOImpl.update(role);
	}

	@Override
	public Role getById(Integer id) {
		return roleDAOImpl.getById(id);
	}

}
