package com.ikubInfo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikubInfo.daoImpl.PermissionDAOImpl;
import com.ikubInfo.pojo.Permission;
import com.ikubInfo.service.PermissionService;

@Service
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private PermissionDAOImpl permissionDAOImpl;

	public List<Permission> getAll() {
		return permissionDAOImpl.getAll();
	}

	@Override
	public List<Permission> getPermissionByRole(int roleId) {
		return permissionDAOImpl.getPermissionByRole(roleId);
	}

	@Override
	public Permission getById(Integer id) {
		return permissionDAOImpl.getById(id);
	}

	@Override
	public void save(Permission permission) {
		permissionDAOImpl.save(permission);

	}

	@Override
	public void update(Permission permission) {
		permissionDAOImpl.update(permission);

	}

}
