package com.ikubInfo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikubInfo.daoImpl.TaskDAOImpl;
import com.ikubInfo.pojo.Task;
import com.ikubInfo.service.TaskService;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskDAOImpl taskDAOImpl;

	public List<Task> getAll() {
		return taskDAOImpl.getAll();
	}

	public void save(Task task) {
		taskDAOImpl.save(task);

	}

	public void update(Task task) {
		taskDAOImpl.update(task);
	}

	@Override
	public void delete(Task task) {
		taskDAOImpl.update(task);
	}

	@Override
	public Task getById(Integer id) {
		return taskDAOImpl.getById(id);
	}

	@Override
	public void deletefromStaffTask(Integer taskId) {
		taskDAOImpl.deletefromStaffTask(taskId);
	}

	@Override
	public List<Task> getTaskByStaffId(Integer staffid) {
		return taskDAOImpl.getTaskByStaffId(staffid);
	}
}
