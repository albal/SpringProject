package com.ikubInfo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikubInfo.daoImpl.ReportDAOImpl;
import com.ikubInfo.pojo.Report;
import com.ikubInfo.service.ReportService;

@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	private ReportDAOImpl reportDAOImpl;

	@Override
	public List<Report> getAll() {
		return reportDAOImpl.getAll();
	}

	@Override
	public void save(Report report) {
		reportDAOImpl.save(report);
	}

	@Override
	public void update(Report report) {
		reportDAOImpl.update(report);
	}

	@Override
	public void delete(Report report) {
		reportDAOImpl.update(report);
	}

}
