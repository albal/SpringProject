package com.ikubInfo.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ikubInfo.converter.ReportConverter;
import com.ikubInfo.entities.ReportEntity;
import com.ikubInfo.pojo.Report;

@Repository
public class ReportDAOImpl {

	public static final int invalid = 0;
	public static final int valid = 1;

	@PersistenceContext
	private EntityManager entityManager;

	private static final ReportConverter REPORT_CONVERTER = new ReportConverter();

	@Transactional(readOnly = true)
	public List<Report> getAll() {

		List<Report> reports = new ArrayList<Report>();
		TypedQuery<ReportEntity> query = entityManager.createQuery("from ReportEntity where validity=?1",
				ReportEntity.class);
		query.setParameter(1, valid);
		for (ReportEntity a : query.getResultList()) {
			reports.add(REPORT_CONVERTER.doBackward(a));
		}
		return reports;
	}

	@Transactional(readOnly = true)
	public List<Report> getAllUserReport(Integer staffId) {

		List<Report> reports = new ArrayList<Report>();
		TypedQuery<ReportEntity> query = entityManager.createQuery(
				"Select s from ReportEntity s join s.staffId t where t.validity=?1 and t.id=?2 and s.validity=?1",
				ReportEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, staffId);
		for (ReportEntity a : query.getResultList()) {
			reports.add(REPORT_CONVERTER.doBackward(a));
		}
		return reports;
	}

	@Transactional
	public void save(Report report) {
		ReportEntity a = REPORT_CONVERTER.doForward(report);
		entityManager.persist(a);
	}

	@Transactional
	public void update(Report report) {
		ReportEntity a = REPORT_CONVERTER.doForward(report);
		entityManager.merge(a);
	}

	@Transactional
	public void delete(Report report) {
		ReportEntity a = REPORT_CONVERTER.doForward(report);
		a.setValidity(invalid);
		entityManager.merge(a);
	}

}
