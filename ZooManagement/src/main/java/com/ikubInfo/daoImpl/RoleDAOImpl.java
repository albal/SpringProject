package com.ikubInfo.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ikubInfo.converter.RoleConverter;
import com.ikubInfo.entities.RoleEntity;
import com.ikubInfo.pojo.Role;

@Repository
public class RoleDAOImpl {

	public static final int invalid = 0;
	public static final int valid = 1;

	@PersistenceContext
	private EntityManager entityManager;

	private static final RoleConverter ROLE_CONVERTER = new RoleConverter();

	@Transactional(readOnly = true)
	public List<Role> getAll() {
		List<Role> roles = new ArrayList<Role>();
		TypedQuery<RoleEntity> query = entityManager.createQuery("from RoleEntity where validity=?1", RoleEntity.class);
		query.setParameter(1, valid);

		for (RoleEntity e : query.getResultList()) {
			roles.add(ROLE_CONVERTER.doBackward(e));
		}
		return roles;

	}

	public Role getById(Integer id) {

		TypedQuery<RoleEntity> query = entityManager.createQuery("from RoleEntity where validity=?1 and id=?2",
				RoleEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, id);
		return ROLE_CONVERTER.doBackward(query.getSingleResult());
	}

	@Transactional
	public void save(Role role) {
		RoleEntity r = ROLE_CONVERTER.doForward(role);
		entityManager.persist(r);
	}

	@Transactional
	public void update(Role role) {
		RoleEntity r = ROLE_CONVERTER.doForward(role);
		entityManager.merge(r);
	}

}
