package com.ikubInfo.daoImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

public abstract class GenericDao<T> {

	public static final int invalid = 0;
	public static final int valid = 1;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public void save(T t) {
		getEntityManager().persist(t);
	}

	@Transactional
	public void update(T t) {
		getEntityManager().merge(t);
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

}
