package com.ikubInfo.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ikubInfo.converter.DailyDietConverter;
import com.ikubInfo.entities.DailyDietEntity;
import com.ikubInfo.pojo.DailyDiet;

@Repository
public class DailyDietDAOImpl {

	public static final int invalid = 0;
	public static final int valid = 1;

	@PersistenceContext
	private EntityManager entityManager;

	private static final DailyDietConverter DAILYDIET_CONVERTER = new DailyDietConverter();

	@Transactional(readOnly = true)
	public List<DailyDiet> getAll() {

		List<DailyDiet> diets = new ArrayList<DailyDiet>();
		TypedQuery<DailyDietEntity> query = entityManager.createQuery("from DailyDietEntity where validity=?1",
				DailyDietEntity.class);
		query.setParameter(1, valid);
		for (DailyDietEntity a : query.getResultList()) {
			diets.add(DAILYDIET_CONVERTER.doBackward(a));
		}
		return diets;
	}

	@Transactional
	public void save(DailyDiet dailyDiet) {
		DailyDietEntity d = DAILYDIET_CONVERTER.doForward(dailyDiet);
		entityManager.persist(d);
	}

	@Transactional
	public void update(DailyDiet dailyDiet) {
		DailyDietEntity d = DAILYDIET_CONVERTER.doForward(dailyDiet);
		entityManager.merge(d);
	}

	@Transactional
	public void delete(DailyDiet dailyDiet) {
		DailyDietEntity d = DAILYDIET_CONVERTER.doForward(dailyDiet);
		d.setValidity(invalid);
		entityManager.merge(d);
	}

}
