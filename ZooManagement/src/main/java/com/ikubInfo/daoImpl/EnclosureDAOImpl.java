package com.ikubInfo.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ikubInfo.converter.EnclosureConverter;
import com.ikubInfo.entities.EnclosureEntity;
import com.ikubInfo.pojo.Enclosure;

@Repository
public class EnclosureDAOImpl{

	public static final int invalid = 0;
	public static final int valid = 1;

	@PersistenceContext
	private EntityManager entityManager;

	private static final EnclosureConverter ENCLOSURE_CONVERTER = new EnclosureConverter();

	@Transactional(readOnly = true)
	public List<Enclosure> getAll() {

		List<Enclosure> enclosure = new ArrayList<Enclosure>();
		TypedQuery<EnclosureEntity> query = entityManager.createQuery("from EnclosureEntity where validity=?1",
				EnclosureEntity.class);
		query.setParameter(1, valid);
		for (EnclosureEntity e : query.getResultList()) {
			enclosure.add(ENCLOSURE_CONVERTER.doBackward(e));
		}
		return enclosure;

	}

	public Enclosure getById(int id) {
		TypedQuery<EnclosureEntity> query = entityManager
				.createQuery("from EnclosureEntity where validity=?1 and id=?2", EnclosureEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, id);
		return ENCLOSURE_CONVERTER.doBackward(query.getSingleResult());

	}

}
