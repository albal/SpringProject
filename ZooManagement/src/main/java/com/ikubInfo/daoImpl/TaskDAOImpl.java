package com.ikubInfo.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ikubInfo.converter.TaskConverter;
import com.ikubInfo.entities.TaskEntity;
import com.ikubInfo.pojo.Task;

@Repository
public class TaskDAOImpl {

	public static final int invalid = 0;
	public static final int valid = 1;
	public static final int done = 1;
	public static final int notDone = 0;

	@PersistenceContext
	private EntityManager entityManager;

	private static final TaskConverter TASK_CONVERTER = new TaskConverter();

	@Transactional(readOnly = true)
	public List<Task> getAll() {

		List<Task> tasks = new ArrayList<Task>();
		TypedQuery<TaskEntity> query = entityManager.createQuery("from TaskEntity where validity=?1 order by id desc",
				TaskEntity.class);
		query.setParameter(1, valid);
		for (TaskEntity a : query.getResultList()) {
			tasks.add(TASK_CONVERTER.doBackward(a));
		}
		return tasks;
	}

	@Transactional(readOnly = true)
	public Task getById(Integer id) {

		TypedQuery<TaskEntity> query = entityManager.createQuery("from TaskEntity where validity=?1 and id=?2",
				TaskEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, id);
		return TASK_CONVERTER.doBackward(query.getSingleResult());

	}

	@Transactional(readOnly = true)
	public List<Task> getTaskByStaffId(Integer staffid) {
		List<Task> tasks = new ArrayList<Task>();
		TypedQuery<TaskEntity> query = entityManager.createQuery(
				"Select t from TaskEntity t where t.validity=?1 and t.id IN (select taskId from StaffTaskEntity where staffId=?2 and status=?3)",
				TaskEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, staffid);
		query.setParameter(3, notDone);
		for (TaskEntity a : query.getResultList()) {
			tasks.add(TASK_CONVERTER.doBackward(a));
		}
		return tasks;

	}

	@Transactional
	public void deletefromStaffTask(Integer taskId) {
		try {
			Query query = entityManager.createQuery("Delete from StaffTaskEntity  where taskId=?1");
			query.setParameter(1, taskId);
			query.executeUpdate();
		} catch (Exception e) {
		}
	}

	@Transactional
	public void delete(Task task) {
		TaskEntity taskEntity = TASK_CONVERTER.doForward(task);
		taskEntity.setValidity(invalid);
		entityManager.merge(taskEntity);
	}

	@Transactional
	public void save(Task task) {
		TaskEntity taskEntity = TASK_CONVERTER.doForward(task);
		entityManager.persist(taskEntity);

	}

	@Transactional
	public void update(Task task) {
		TaskEntity taskEntity = TASK_CONVERTER.doForward(task);
		entityManager.merge(taskEntity);

	}
}
