package com.ikubInfo.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ikubInfo.converter.StaffConverter;
import com.ikubInfo.converter.StaffTaskConverter;
import com.ikubInfo.entities.StaffEntity;
import com.ikubInfo.entities.StaffTaskEntity;
import com.ikubInfo.pojo.Staff;
import com.ikubInfo.pojo.StaffTask;
import com.ikubInfo.util.PasswordSecurity;

@Repository
public class StaffDAOImpl  {

	public static final int invalid = 0;
	public static final int valid = 1;

	@PersistenceContext
	private EntityManager entityManager;

	private static final StaffConverter STAFF_CONVERTER = new StaffConverter();
	private static final StaffTaskConverter STAFFTASK_CONVERTER = new StaffTaskConverter();

	@Transactional(readOnly = true)
	public List<Staff> getAll() {

		List<Staff> staff = new ArrayList<Staff>();
		TypedQuery<StaffEntity> query = entityManager.createQuery("from StaffEntity where validity=?1 order by id desc",
				StaffEntity.class);
		query.setParameter(1, valid);
		for (StaffEntity s : query.getResultList()) {
			staff.add(STAFF_CONVERTER.doBackward(s));
		}
		return staff;

	}

	@Transactional(readOnly = true)
	public Staff getById(Integer id) {
		TypedQuery<StaffEntity> query = entityManager.createQuery("from StaffEntity where validity=?1 and id=?2",
				StaffEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, id);
		return STAFF_CONVERTER.doBackward(query.getSingleResult());

	}

	@Transactional(readOnly = true)
	public Staff getbyUsername(String username) {
		TypedQuery<StaffEntity> query = entityManager.createQuery("from StaffEntity where validity=?1 and username=?2",
				StaffEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, username);
		try {
			return STAFF_CONVERTER.doBackward(query.getSingleResult());
		} catch (Exception e) {
			return null;
		}
	}

	public boolean existUsername(String username) {
		TypedQuery<Long> query = entityManager
				.createQuery("select count(s.id) from StaffEntity s where s.validity=?1 and s.username=?2", Long.class);
		query.setParameter(1, valid);
		query.setParameter(2, username);
		Long check = query.getSingleResult();
		if (check == 0) {
			return false;
		} else {
			return true;
		}
	}

	@Transactional(readOnly = true)
	public List<Staff> getStaffByRole(String title) {

		List<Staff> staff = new ArrayList<Staff>();
		TypedQuery<StaffEntity> query = entityManager.createQuery(
				"Select s from StaffEntity s join s.role r where s.validity=?1 and r.title=?2 and r.validity=?1",
				StaffEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, title);
		for (StaffEntity s : query.getResultList()) {
			staff.add(STAFF_CONVERTER.doBackward(s));
		}
		return staff;

	}

	@Transactional(readOnly = true)
	public Staff checklogin(String username, String password) {
		try {
			TypedQuery<StaffEntity> query = entityManager.createQuery(
					"from StaffEntity  where LOWER(username)=LOWER(?1) and password=?2 and validity=?3",
					StaffEntity.class);
			query.setParameter(1, username);
			query.setParameter(2, password);
			query.setParameter(3, valid);
			return STAFF_CONVERTER.doBackward(query.getSingleResult());
		} catch (Exception e) {
			return null;
		}
	}

	@Transactional
	public void saveStaffInTask(StaffTask staffTask) {
		StaffTaskEntity staffEntity = STAFFTASK_CONVERTER.doForward(staffTask);
		entityManager.persist(staffEntity);
	}

	@Transactional
	public void save(Staff staff) {
		StaffEntity staffEntity = STAFF_CONVERTER.doForward(staff);
		staffEntity.setPassword(PasswordSecurity.encrypt(staff.getPassword()));
		entityManager.persist(staffEntity);
	}

	@Transactional
	public void update(Staff staff) {
		StaffEntity staffEntity = STAFF_CONVERTER.doForward(staff);
		entityManager.merge(staffEntity);
	}

	@Transactional
	public void delete(Staff staff) {
		StaffEntity staffEntity = STAFF_CONVERTER.doForward(staff);
		staffEntity.setValidity(invalid);
		entityManager.merge(staffEntity);
	}

}
