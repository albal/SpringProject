package com.ikubInfo.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ikubInfo.converter.AnimalCategoryConverter;
import com.ikubInfo.entities.AnimalCategoryEntity;
import com.ikubInfo.pojo.AnimalCategory;

@Repository
public class AnimalCategoryDAOImpl {

	public static final int invalid = 0;
	public static final int valid = 1;

	@PersistenceContext
	private EntityManager entityManager;

	private static final AnimalCategoryConverter ANIMALCATEGORY_CONVERTER = new AnimalCategoryConverter();

	@Transactional(readOnly = true)
	public List<AnimalCategory> getAll() {
		List<AnimalCategory> animalCategories = new ArrayList<AnimalCategory>();
		TypedQuery<AnimalCategoryEntity> query = entityManager
				.createQuery("from AnimalCategoryEntity where validity=?1", AnimalCategoryEntity.class);
		query.setParameter(1, valid);

		for (AnimalCategoryEntity e : query.getResultList()) {
			animalCategories.add(ANIMALCATEGORY_CONVERTER.doBackward(e));
		}
		return animalCategories;

	}

	public AnimalCategory getById(Integer id) {

		TypedQuery<AnimalCategoryEntity> query = entityManager
				.createQuery("from AnimalCategoryEntity where validity=?1 and id=?2", AnimalCategoryEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, id);
		return ANIMALCATEGORY_CONVERTER.doBackward(query.getSingleResult());

	}

}