package com.ikubInfo.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ikubInfo.converter.AnimalConverter;
import com.ikubInfo.entities.AnimalEntity;
import com.ikubInfo.pojo.Animal;

@Repository
public class AnimalDAOImpl {

	public static final int invalid = 0;
	public static final int valid = 1;

	@PersistenceContext
	private EntityManager entityManager;

	private static final AnimalConverter ANIMAL_CONVERTER = new AnimalConverter();

	@Transactional(readOnly = true)
	public List<Animal> getAll() {

		List<Animal> animals = new ArrayList<Animal>();
		TypedQuery<AnimalEntity> query = entityManager
				.createQuery("from AnimalEntity where validity=?1 order by id desc", AnimalEntity.class);
		query.setParameter(1, valid);
		for (AnimalEntity a : query.getResultList()) {
			animals.add(ANIMAL_CONVERTER.doBackward(a));
		}
		return animals;
	}

	public Animal getById(Integer id) {

		TypedQuery<AnimalEntity> query = entityManager.createQuery("from AnimalEntity where validity=?1 and id=?2",
				AnimalEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, id);
		return ANIMAL_CONVERTER.doBackward(query.getSingleResult());

	}

	@Transactional(readOnly = true)
	public boolean existScientificName(String scientificName) {
		TypedQuery<Long> query = entityManager.createQuery(
				"select count(a.id) from AnimalEntity a where a.validity=?1 and a.scientificName=?2", Long.class);
		query.setParameter(1, valid);
		query.setParameter(2, scientificName);
		Long check = query.getSingleResult();
		if (check == 0) {
			return false;
		} else {
			return true;
		}
	}

	public Animal getByName(String name) {
		TypedQuery<AnimalEntity> query = entityManager.createQuery("from AnimalEntity where validity=?1 and name=?2",
				AnimalEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, name);
		return ANIMAL_CONVERTER.doBackward(query.getSingleResult());

	}

	@Transactional(readOnly = true)
	public Long animalsInEnclosure(String name) {
		Query query = entityManager.createQuery(
				"Select count(e.id) from AnimalEntity a join a.enclosureId e where a.validity=?1 and e.type=?2 and e.validity=?1");
		query.setParameter(1, valid);
		query.setParameter(2, name);
		return (Long) query.getSingleResult();
	}

	@Transactional
	public void save(Animal animal) {
		AnimalEntity a = ANIMAL_CONVERTER.doForward(animal);
		entityManager.persist(a);
	}

	@Transactional
	public void update(Animal animal) {
		AnimalEntity a = ANIMAL_CONVERTER.doForward(animal);
		entityManager.merge(a);
	}

	@Transactional
	public void delete(Animal animal) {
		AnimalEntity a = ANIMAL_CONVERTER.doForward(animal);
		a.setValidity(invalid);
		entityManager.merge(a);
	}
}
