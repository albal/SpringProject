package com.ikubInfo.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ikubInfo.converter.PermissionConverter;
import com.ikubInfo.entities.PermissionEntity;
import com.ikubInfo.pojo.Permission;

@Repository
public class PermissionDAOImpl {

	public static final int invalid = 0;
	public static final int valid = 1;

	@PersistenceContext
	private EntityManager entityManager;

	private static final PermissionConverter PERMISSION_CONVERTER = new PermissionConverter();

	@Transactional(readOnly = true)
	public List<Permission> getAll() {

		List<Permission> permissions = new ArrayList<Permission>();
		TypedQuery<PermissionEntity> query = entityManager.createQuery("from PermissionEntity where validity=?1",
				PermissionEntity.class);
		query.setParameter(1, valid);
		for (PermissionEntity a : query.getResultList()) {
			permissions.add(PERMISSION_CONVERTER.doBackward(a));
		}
		return permissions;
	}

	public Permission getById(Integer id) {

		TypedQuery<PermissionEntity> query = entityManager
				.createQuery("from PermissionEntity where validity=?1 and id=?2", PermissionEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, id);
		return PERMISSION_CONVERTER.doBackward(query.getSingleResult());
	}

	@Transactional(readOnly = true)
	public List<Permission> getPermissionByRole(int roleId) {

		List<Permission> permissions = new ArrayList<Permission>();
		TypedQuery<PermissionEntity> query = entityManager.createQuery(
				"Select p from RoleEntity r join r.permission p where r.validity=?1 and r.id=?2",
				PermissionEntity.class);
		query.setParameter(1, valid);
		query.setParameter(2, roleId);

		for (PermissionEntity e : query.getResultList()) {
			permissions.add(PERMISSION_CONVERTER.doBackward(e));
		}
		return permissions;
	}

	@Transactional
	public void save(Permission permission) {
		PermissionEntity p = PERMISSION_CONVERTER.doForward(permission);
		entityManager.persist(p);
	}

	@Transactional
	public void update(Permission permission) {
		PermissionEntity p = PERMISSION_CONVERTER.doForward(permission);
		entityManager.merge(p);
	}

}
