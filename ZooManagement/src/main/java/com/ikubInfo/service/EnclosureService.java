package com.ikubInfo.service;

import java.util.List;

import com.ikubInfo.pojo.Enclosure;

public interface EnclosureService {

	public List<Enclosure> getAll();

	public Enclosure getById(int id);

}
