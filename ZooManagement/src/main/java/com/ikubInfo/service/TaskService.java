package com.ikubInfo.service;

import java.util.List;

import com.ikubInfo.pojo.Task;

public interface TaskService {

	public List<Task> getAll();

	public void save(Task task);

	public void update(Task task);

	public void delete(Task task);

	public Task getById(Integer id);

	public void deletefromStaffTask(Integer taskId);

	public List<Task> getTaskByStaffId(Integer staffid);

}
