package com.ikubInfo.service;

import java.util.List;

import com.ikubInfo.pojo.Role;

public interface RoleService {

	public List<Role> getAll();

	public void save(Role role);

	public void update(Role role);

	public Role getById(Integer id);

}
