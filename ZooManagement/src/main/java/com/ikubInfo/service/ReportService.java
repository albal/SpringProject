package com.ikubInfo.service;

import java.util.List;

import com.ikubInfo.pojo.Report;

public interface ReportService {
	
	public List<Report> getAll();

	public void save(Report report);

	public void update(Report report);

	public void delete(Report report);

}
