package com.ikubInfo.service;

import java.util.List;

import com.ikubInfo.pojo.DailyDiet;

public interface DailyDietService {

	public List<DailyDiet> getAll();

	public void save(DailyDiet dailyDiet);

	public void update(DailyDiet dailyDiet);

	public void delete(DailyDiet dailyDiet);

}
