package com.ikubInfo.service;

import java.util.List;

import com.ikubInfo.pojo.Staff;
import com.ikubInfo.pojo.StaffTask;

public interface StaffService {

	public List<Staff> getAll();

	public Staff getById(Integer id);

	public void save(Staff staff);

	public void update(Staff staff);

	public void delete(Staff staff);

	public boolean existUsername(String username);

	public Staff getbyUsername(String username);

	public Staff login(String username, String password);

	public List<Staff> getStaffByRole();

	public void saveStaffInTask(StaffTask staffTask);

}
