package com.ikubInfo.service;

import java.util.List;

import com.ikubInfo.pojo.AnimalCategory;

public interface AnimalCategoryService {

	public List<AnimalCategory> getAll();

	public AnimalCategory getById(Integer id);

}
