package com.ikubInfo.service;

import java.util.List;

import com.ikubInfo.pojo.Permission;

public interface PermissionService {

	public List<Permission> getAll();

	public void save(Permission permission);

	public void update(Permission permission);

	public List<Permission> getPermissionByRole(int roleId);

	public Permission getById(Integer id);

}
