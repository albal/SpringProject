package com.ikubInfo.service;

import java.util.List;

import com.ikubInfo.pojo.Animal;

public interface AnimalService {

	public List<Animal> getAll();

	public Animal getById(Integer id);

	public void save(Animal animal);

	public void update(Animal animal);

	public void delete(Animal animal);
	
	public boolean existScientificName(String name);
	
	public Animal getByName(String name);
	
	public Long animalsInEnclosure(String name);

}
