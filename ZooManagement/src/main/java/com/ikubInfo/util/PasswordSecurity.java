package com.ikubInfo.util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class PasswordSecurity {

	public static String encrypt(String noencrypted) {
		String md5 = null;
		try {
			MessageDigest mdEnc = MessageDigest.getInstance("MD5"); // Encryption algorithm
			mdEnc.update(noencrypted.getBytes(), 0, noencrypted.length());
			md5 = new BigInteger(1, mdEnc.digest()).toString(16); // Encrypted string
		} catch (Exception ex) {
			return null;
		}
		return md5;
	}
}
