package com.ikubInfo.util;

public enum Roles {

	ADMIN("admin"), ZOOKEEPER("zookeeper"), MANAGER("manager");

	public String name;

	Roles(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static Roles getRoleByName(String name) {
		for (Roles rol : values())
			if (rol.getName().equals(name)) {
				return rol;
			}
		return null;
	}

}
