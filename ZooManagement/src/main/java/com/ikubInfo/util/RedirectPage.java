package com.ikubInfo.util;

public class RedirectPage {

	public static String redirectToLogin() {
		return "/login.xhtml?faces-redirect=true";
	}

	public static String redirectToManager() {
		return "/private/shared/taskDataTable.xhtml?faces-redirect=true";
	}

	public static String redirectToAdmin() {
		return "/private/admin/usersDataTable.xhtml?faces-redirect=true";
	}

	public static String redirectToZookeeper() {
		return "/private/zookeeper/tasksTable.xhtml?faces-redirect=true";
	}

}
