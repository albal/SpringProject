package com.ikubInfo.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.omnifaces.util.selectitems.SelectItemsUtils;

@FacesConverter(value = "converterClass")
public class DropDownConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String val) {
		return SelectItemsUtils.findValueByStringConversion(context, component, val, this);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(object);
		} else {
			return null;
		}
	}

}
