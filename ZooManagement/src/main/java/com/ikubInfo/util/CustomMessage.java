package com.ikubInfo.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class CustomMessage {
	
	public static void addInfoMsg(String msg) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
	}

	public static void addErrorMsg(String msg) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
	}

}
