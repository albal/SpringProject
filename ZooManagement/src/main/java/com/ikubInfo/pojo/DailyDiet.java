package com.ikubInfo.pojo;

import java.util.Date;

public class DailyDiet {

	private Integer id;
	private String foodType;
	private String unitMass;
	private Integer foodPortion;
	private Date startDate;
	private Date endDate;
	private Staff createdBy;
	private Integer validity;
	private Animal animal;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFoodType() {
		return foodType;
	}

	public void setFoodType(String foodType) {
		this.foodType = foodType;
	}

	public String getUnitMass() {
		return unitMass;
	}

	public void setUnitMass(String unitMass) {
		this.unitMass = unitMass;
	}

	public Integer getFoodPortion() {
		return foodPortion;
	}

	public void setFoodPortion(Integer foodPortion) {
		this.foodPortion = foodPortion;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Staff getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Staff createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((animal == null) ? 0 : animal.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((foodPortion == null) ? 0 : foodPortion.hashCode());
		result = prime * result + ((foodType == null) ? 0 : foodType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((unitMass == null) ? 0 : unitMass.hashCode());
		result = prime * result + ((validity == null) ? 0 : validity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DailyDiet other = (DailyDiet) obj;
		if (animal == null) {
			if (other.animal != null)
				return false;
		} else if (!animal.equals(other.animal))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (foodPortion == null) {
			if (other.foodPortion != null)
				return false;
		} else if (!foodPortion.equals(other.foodPortion))
			return false;
		if (foodType == null) {
			if (other.foodType != null)
				return false;
		} else if (!foodType.equals(other.foodType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (unitMass == null) {
			if (other.unitMass != null)
				return false;
		} else if (!unitMass.equals(other.unitMass))
			return false;
		if (validity == null) {
			if (other.validity != null)
				return false;
		} else if (!validity.equals(other.validity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DailyDiet [id=" + id + ", foodType=" + foodType + ", unitMass=" + unitMass + ", foodPortion="
				+ foodPortion + ", startDate=" + startDate + ", endDate=" + endDate + ", createdBy=" + createdBy
				+ ", validity=" + validity + ", animal=" + animal + "]";
	}

}
