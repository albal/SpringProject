package com.ikubInfo.pojo;

import java.util.Date;

public class Report {

	private Integer id;
	private String medicalObservation;
	private String dietObservation;
	private String comments;
	private Date createdDate;
	private Integer validity;
	private StaffTask staffTaskId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMedicalObservation() {
		return medicalObservation;
	}

	public void setMedicalObservation(String medicalObservation) {
		this.medicalObservation = medicalObservation;
	}

	public String getDietObservation() {
		return dietObservation;
	}

	public void setDietObservation(String dietObservation) {
		this.dietObservation = dietObservation;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public StaffTask getStaffTaskId() {
		return staffTaskId;
	}

	public void setStaffTaskId(StaffTask staffTaskId) {
		this.staffTaskId = staffTaskId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((dietObservation == null) ? 0 : dietObservation.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((medicalObservation == null) ? 0 : medicalObservation.hashCode());
		result = prime * result + ((staffTaskId == null) ? 0 : staffTaskId.hashCode());
		result = prime * result + ((validity == null) ? 0 : validity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Report other = (Report) obj;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (dietObservation == null) {
			if (other.dietObservation != null)
				return false;
		} else if (!dietObservation.equals(other.dietObservation))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (medicalObservation == null) {
			if (other.medicalObservation != null)
				return false;
		} else if (!medicalObservation.equals(other.medicalObservation))
			return false;
		if (staffTaskId == null) {
			if (other.staffTaskId != null)
				return false;
		} else if (!staffTaskId.equals(other.staffTaskId))
			return false;
		if (validity == null) {
			if (other.validity != null)
				return false;
		} else if (!validity.equals(other.validity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Report [id=" + id + ", medicalObservation=" + medicalObservation + ", dietObservation="
				+ dietObservation + ", comments=" + comments + ", createdDate=" + createdDate + ", validity=" + validity
				+ ", staffTaskId=" + staffTaskId + "]";
	}

}
