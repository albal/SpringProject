package com.ikubInfo.pojo;

import java.util.Date;

public class Animal {

	private Integer id;
	private String name;
	private String scientificName;
	private Integer age;
	private double weight;
	private String gender;
	private Date arrivalDate;
	private Date createdDate;
	private Integer validity;
	private AnimalCategory animalCategoryId;
	private Enclosure enclosureId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScientificName() {
		return scientificName;
	}

	public void setScientificName(String scientificName) {
		this.scientificName = scientificName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public AnimalCategory getAnimalCategoryId() {
		return animalCategoryId;
	}

	public void setAnimalCategoryId(AnimalCategory animalCategoryId) {
		this.animalCategoryId = animalCategoryId;
	}

	public Enclosure getEnclosureId() {
		return enclosureId;
	}

	public void setEnclosureId(Enclosure enclosureId) {
		this.enclosureId = enclosureId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result + ((animalCategoryId == null) ? 0 : animalCategoryId.hashCode());
		result = prime * result + ((arrivalDate == null) ? 0 : arrivalDate.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((enclosureId == null) ? 0 : enclosureId.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((scientificName == null) ? 0 : scientificName.hashCode());
		result = prime * result + ((validity == null) ? 0 : validity.hashCode());
		long temp;
		temp = Double.doubleToLongBits(weight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animal other = (Animal) obj;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (animalCategoryId == null) {
			if (other.animalCategoryId != null)
				return false;
		} else if (!animalCategoryId.equals(other.animalCategoryId))
			return false;
		if (arrivalDate == null) {
			if (other.arrivalDate != null)
				return false;
		} else if (!arrivalDate.equals(other.arrivalDate))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (enclosureId == null) {
			if (other.enclosureId != null)
				return false;
		} else if (!enclosureId.equals(other.enclosureId))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (scientificName == null) {
			if (other.scientificName != null)
				return false;
		} else if (!scientificName.equals(other.scientificName))
			return false;
		if (validity == null) {
			if (other.validity != null)
				return false;
		} else if (!validity.equals(other.validity))
			return false;
		if (Double.doubleToLongBits(weight) != Double.doubleToLongBits(other.weight))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Animal [id=" + id + ", name=" + name + ", scientificName=" + scientificName + ", age=" + age
				+ ", weight=" + weight + ", gender=" + gender + ", arrivalDate=" + arrivalDate + ", createdDate="
				+ createdDate + ", validity=" + validity + ", animalCategoryId=" + animalCategoryId + ", enclosureId="
				+ enclosureId + "]";
	}

}
