package com.ikubInfo.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.ikubInfo.pojo.Permission;
import com.ikubInfo.pojo.Staff;
import com.ikubInfo.service.PermissionService;

@ManagedBean
@SessionScoped
public class SessionManager implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{permissionServiceImpl}")
	private PermissionService permissionService;

	private Staff staf;

	public boolean isAdmin() {
		if (staf.getRole().getTitle().equals("admin")) {
			return true;
		}
		return false;
	}

	public boolean isZookeeper() {
		if (staf.getRole().getTitle().equals("zookeeper")) {
			return true;
		}
		return false;
	}

	public boolean isManager() {
		if (staf.getRole().getTitle().equals("manager")) {
			return true;
		}
		return false;
	}

	public Map<String, Permission> getPermission() {
		Map<String, Permission> mapPermission = new HashMap<>();
		List<Permission> permissions = permissionService.getPermissionByRole(this.staf.getRole().getId());
		for (Permission p : permissions) {
			mapPermission.put(p.getName(), p);
		}
		return mapPermission;
	}

	public boolean hasUserPermission(String permission) {
		return getPermission().get(permission) != null;
	}

	public Staff getStaf() {
		return staf;
	}

	public void setStaf(Staff staf) {
		this.staf = staf;
	}

	public PermissionService getPermissionService() {
		return permissionService;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}

}
