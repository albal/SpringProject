package com.ikubInfo.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.HttpSession;

import com.ikubInfo.pojo.Staff;
import com.ikubInfo.service.StaffService;
import com.ikubInfo.util.CustomMessage;
import com.ikubInfo.util.PasswordSecurity;
import com.ikubInfo.util.RedirectPage;
import com.ikubInfo.util.Roles;
import com.ikubInfo.util.SessionUtil;

@ManagedBean
@RequestScoped
public class LoginBean {

	private String username;
	private String password;

	@ManagedProperty("#{staffServiceImpl}")
	private StaffService staffService;

	@ManagedProperty("#{sessionManager}")
	private SessionManager sessionManager;

	public String login() {
		HttpSession session = SessionUtil.getSession();
		Staff staff = staffService.login(username, PasswordSecurity.encrypt(password));
		if (staff != null) {
			session.setAttribute("user", staff);
			sessionManager.setStaf(staff);
			String role = staff.getRole().getTitle();
			Roles rol = Roles.getRoleByName(role);
			switch (rol) {
			case ADMIN:
				return RedirectPage.redirectToAdmin();
			case MANAGER:
				return RedirectPage.redirectToManager();
			case ZOOKEEPER:
				return RedirectPage.redirectToZookeeper();
			default:
				return RedirectPage.redirectToLogin();
			}
		} else {
			CustomMessage.addErrorMsg("Username or password incorrect!");
			return null;
		}
	}

	public String logout() {
		HttpSession session = SessionUtil.getSession();
		session.removeAttribute("user");
		session.invalidate();
		return RedirectPage.redirectToLogin();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public StaffService getStaffService() {
		return staffService;
	}

	public void setStaffService(StaffService staffService) {
		this.staffService = staffService;
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

}
