package com.ikubInfo.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ikubInfo.pojo.DailyDiet;
import com.ikubInfo.service.DailyDietService;
import com.ikubInfo.util.CustomMessage;

@ViewScoped
@ManagedBean
public class DailyDietBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{dailyDietServiceImpl}")
	private DailyDietService dailyDietService;

	private List<DailyDiet> diets;
	private DailyDiet diet = new DailyDiet();

	@PostConstruct
	public void init() {
		diets = dailyDietService.getAll();

	}

	public void save() {
		try {
			dailyDietService.save(diet);
			CustomMessage.addInfoMsg("Diet was added with success!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while adding diet!");
		}
	}

	public void update() {
		try {
			dailyDietService.update(diet);
			CustomMessage.addInfoMsg("Diet was updated with success!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while updating diet!");
		}
	}

	public void delete(DailyDiet dailyDiet) {
		try {
			dailyDietService.delete(dailyDiet);
			diets.remove(dailyDiet);
			CustomMessage.addInfoMsg("Task was deleted with success!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while deleting task!");
		}
	}

	public void loadDiet() {
		diets.clear();
		diets.addAll(dailyDietService.getAll());
	}

	public void reset() {
		this.diet = new DailyDiet();
	}

	public DailyDietService getDailyDietService() {
		return dailyDietService;
	}

	public void setDailyDietService(DailyDietService dailyDietService) {
		this.dailyDietService = dailyDietService;
	}

	public List<DailyDiet> getDiets() {
		return diets;
	}

	public void setDiets(List<DailyDiet> diets) {
		this.diets = diets;
	}

	public DailyDiet getDiet() {
		return diet;
	}

	public void setDiet(DailyDiet diet) {
		this.diet = diet;
	}

}
