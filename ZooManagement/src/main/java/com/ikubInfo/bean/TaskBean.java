package com.ikubInfo.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ikubInfo.pojo.Report;
import com.ikubInfo.pojo.Staff;
import com.ikubInfo.pojo.StaffTask;
import com.ikubInfo.pojo.Task;
import com.ikubInfo.service.ReportService;
import com.ikubInfo.service.StaffService;
import com.ikubInfo.service.TaskService;
import com.ikubInfo.util.CustomMessage;

@ManagedBean
@ViewScoped
public class TaskBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{taskServiceImpl}")
	private TaskService taskService;

	@ManagedProperty(value = "#{staffServiceImpl}")
	private StaffService staffService;

	@ManagedProperty(value = "#{reportServiceImpl}")
	private ReportService reportService;

	@ManagedProperty(value = "#{sessionManager}")
	private SessionManager sessionManager;

	private List<Report> reportList;
	private Report report = new Report();

	private List<Task> taskList;
	private Task selectedtask = new Task();

	private List<Task> staffTask;

	private StaffTask staffTaskId = new StaffTask();
	private List<Staff> staffList;
	private boolean value;

	@PostConstruct
	public void init() {
		reportList = reportService.getAll();
		staffList = staffService.getStaffByRole();
		if (sessionManager.isZookeeper()) {
			staffTask = taskService.getTaskByStaffId(sessionManager.getStaf().getId());
		} else {
			taskList = taskService.getAll();
		}
	}

	public void saveReport() {
		try {
			staffTaskId.setStaffId(sessionManager.getStaf().getId());
			staffTaskId.setTaskId(selectedtask.getId());
			report.setStaffTaskId(staffTaskId);
			reportService.save(report);
			CustomMessage.addInfoMsg("Raport was saved succesfully!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops!  Looks like something went wrong while inserting raport!");
		}

	}

	public void saveStaffTask() {
		try {
			staffTaskId.setStaffId(staffTaskId.getStaffId());
			staffTaskId.setTaskId(selectedtask.getId());
			staffService.saveStaffInTask(staffTaskId);
			CustomMessage.addInfoMsg("Task was assigned to user succesfully!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! User have this task already!");
		}
	}

	public void save() {
		try {
			taskService.save(selectedtask);
			taskList = taskService.getAll();
			CustomMessage.addInfoMsg("Task was added succesfully!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while inserting task!");
		}
	}

	public void update() {
		try {
			taskService.update(selectedtask);
			CustomMessage.addInfoMsg("Task was updated succesfully!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while updating task!");
		}
	}

	public void delete(Task task) {
		try {
			taskService.deletefromStaffTask(selectedtask.getId());
			taskService.delete(task);
			taskList.remove(task);
			CustomMessage.addInfoMsg("Task was deleted with success!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while deleting task!");
		}
	}

	public void loadTask() {
		taskList.clear();
		taskList.addAll(taskService.getAll());
	}

	public void reset() {
		this.selectedtask = new Task();
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public StaffService getStaffService() {
		return staffService;
	}

	public void setStaffService(StaffService staffService) {
		this.staffService = staffService;
	}

	public List<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}

	public Task getSelectedtask() {
		return selectedtask;
	}

	public void setSelectedtask(Task selectedtask) {
		this.selectedtask = selectedtask;
	}

	public ReportService getReportService() {
		return reportService;
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

	public List<Report> getReportList() {
		return reportList;
	}

	public void setReportList(List<Report> reportList) {
		this.reportList = reportList;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public List<Task> getStaffTask() {
		return staffTask;
	}

	public void setStaffTask(List<Task> staffTask) {
		this.staffTask = staffTask;
	}

	public StaffTask getStaffTaskId() {
		return staffTaskId;
	}

	public void setStaffTaskId(StaffTask staffTaskId) {
		this.staffTaskId = staffTaskId;
	}

	public SessionManager getSessionManager() {
		return sessionManager;
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	public List<Staff> getStaffList() {
		return staffList;
	}

	public void setStaffList(List<Staff> staffList) {
		this.staffList = staffList;
	}

}
