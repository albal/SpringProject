package com.ikubInfo.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.ikubInfo.pojo.Animal;
import com.ikubInfo.pojo.AnimalCategory;
import com.ikubInfo.pojo.Enclosure;
import com.ikubInfo.service.AnimalCategoryService;
import com.ikubInfo.service.AnimalService;
import com.ikubInfo.service.EnclosureService;
import com.ikubInfo.util.CustomMessage;

@ViewScoped
@ManagedBean
public class AnimalBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{animalServiceImpl}")
	private AnimalService animalService;

	@ManagedProperty(value = "#{animalCategoryServiceImpl}")
	private AnimalCategoryService animalCategoryService;

	@ManagedProperty(value = "#{enclosureServiceImpl}")
	private EnclosureService enclosureService;

	private List<Animal> animalList;
	private Animal selectedAnimal = new Animal();

	private List<AnimalCategory> animalCategoryList;
	private AnimalCategory animalCategory = new AnimalCategory();

	private List<Enclosure> enclosureList;
	private Enclosure enclosure;

	private Animal animal = new Animal();

	@PostConstruct
	public void init() {

		animalList = animalService.getAll();
		animalCategoryList = animalCategoryService.getAll();
		enclosureList = enclosureService.getAll();
	}

	public void delete(Animal animal) {
		try {
			animalService.delete(animal);
			animalList.remove(animal);
			CustomMessage.addInfoMsg("Animal was deleted with success!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while deleting animal!");
		}
	}

	public void update() {
		try {
			animalService.update(selectedAnimal);
			CustomMessage.addInfoMsg("Animal was updated with success!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while updating animal!");
		}
	}

	public void save() {
		try {
			if (animalService.animalsInEnclosure(selectedAnimal.getEnclosureId().getType()) >= selectedAnimal
					.getEnclosureId().getCapacity()) {
				CustomMessage.addInfoMsg("The enclosure is full!");

			} else {
				animalService.save(selectedAnimal);
				animalList = animalService.getAll();
				CustomMessage.addInfoMsg("Animal was inserted with success!");
			}
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while inserting animal!");
		}
	}

	public void refresh() throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
	}

	public void reset() {
		this.selectedAnimal = new Animal();
		this.animalCategory = new AnimalCategory();
	}

	public void loadAnimal() {
		animalList.clear();
		animalList.addAll(animalService.getAll());
	}

	public AnimalService getAnimalService() {
		return animalService;
	}

	public void setAnimalService(AnimalService animalService) {
		this.animalService = animalService;
	}

	public AnimalCategoryService getAnimalCategoryService() {
		return animalCategoryService;
	}

	public void setAnimalCategoryService(AnimalCategoryService animalCategoryService) {
		this.animalCategoryService = animalCategoryService;
	}

	public List<Animal> getAnimalList() {
		return animalList;
	}

	public void setAnimalList(List<Animal> animalList) {
		this.animalList = animalList;
	}

	public Animal getSelectedAnimal() {
		return selectedAnimal;
	}

	public void setSelectedAnimal(Animal selectedAnimal) {
		this.selectedAnimal = selectedAnimal;
	}

	public List<AnimalCategory> getAnimalCategoryList() {
		return animalCategoryList;
	}

	public void setAnimalCategoryList(List<AnimalCategory> animalCategoryList) {
		this.animalCategoryList = animalCategoryList;
	}

	public AnimalCategory getAnimalCategory() {
		return animalCategory;
	}

	public void setAnimalCategory(AnimalCategory animalCategory) {
		this.animalCategory = animalCategory;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public EnclosureService getEnclosureService() {
		return enclosureService;
	}

	public void setEnclosureService(EnclosureService enclosureService) {
		this.enclosureService = enclosureService;
	}

	public List<Enclosure> getEnclosureList() {
		return enclosureList;
	}

	public void setEnclosureList(List<Enclosure> enclosureList) {
		this.enclosureList = enclosureList;
	}

	public Enclosure getEnclosure() {
		return enclosure;
	}

	public void setEnclosure(Enclosure enclosure) {
		this.enclosure = enclosure;
	}

}
