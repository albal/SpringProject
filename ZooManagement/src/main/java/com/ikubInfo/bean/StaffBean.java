package com.ikubInfo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ikubInfo.pojo.Role;
import com.ikubInfo.pojo.Staff;
import com.ikubInfo.service.RoleService;
import com.ikubInfo.service.StaffService;
import com.ikubInfo.util.CustomMessage;
import com.ikubInfo.util.PasswordSecurity;

@ManagedBean
@ViewScoped
public class StaffBean implements Serializable {

	private static final long serialVersionUID = 1L;

	final static Logger LOGGER = LogManager.getLogger(StaffBean.class);

	@ManagedProperty(value = "#{staffServiceImpl}")
	private StaffService staffService;

	@ManagedProperty(value = "#{roleServiceImpl}")
	private RoleService roleService;

	@ManagedProperty(value = "#{sessionManager}")
	private SessionManager sessionManager;

	private List<Staff> staffList;
	private Staff selected = new Staff();

	private List<Role> roleList;
	private Role role = new Role();

	private List<Staff> searchName = new ArrayList<>();
	private String searchUser;

	private String newPassword;

	@PostConstruct
	public void init() {
		staffList = staffService.getAll();
		roleList = roleService.getAll();
	}

	public void delete(Staff staf) {
		try {
			staffService.delete(staf);
			staffList.remove(staf);
			CustomMessage.addInfoMsg("User was deleted with success!");
		} catch (Exception e) {
			CustomMessage.addInfoMsg("Oops! Looks like something went wrong while deleting user!");
		}
	}

	public void updateProfile() {
		try {
			staffService.update(sessionManager.getStaf());
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while updating profile!");
		}
	}

	public void update() {
		try {
			LOGGER.info("Started updating user");
			staffService.update(selected);
			LOGGER.info("Finished updating user");
			CustomMessage.addInfoMsg("User was updated with success!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while updating user!");
		}

	}

	public void save() {
		try {
			LOGGER.info("Started adding user");
			if (staffService.existUsername(this.selected.getUsername())) {
				CustomMessage.addInfoMsg("User with given username ,already exist!");
			} else {
				staffService.save(selected);
				LOGGER.info("User" + selected + "added");
				CustomMessage.addInfoMsg("User was inserted with success!");
			}
		} catch (Exception e) {
			LOGGER.debug("Sth happened to" + selected.getUsername());
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while inserting user!");
		}
	}

	public void loadUser() {
		staffList.clear();
		staffList.addAll(staffService.getAll());
	}

	public void reset() {
		this.selected = new Staff();
		this.role = new Role();
	}

	public void changePassword() {
		try {
			sessionManager.getStaf().setPassword(PasswordSecurity.encrypt(newPassword));
			staffService.update(sessionManager.getStaf());
			CustomMessage.addInfoMsg("Password changed with success!");
		} catch (Exception e) {
			CustomMessage.addErrorMsg("Oops! Looks like something went wrong while changing password!");
		}
	}

	public StaffService getStaffService() {
		return staffService;
	}

	public void setStaffService(StaffService staffService) {
		this.staffService = staffService;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public List<Staff> getStaffList() {
		return staffList;
	}

	public void setStaffList(List<Staff> staffList) {
		this.staffList = staffList;
	}

	public Staff getSelected() {
		return selected;
	}

	public void setSelected(Staff selected) {
		this.selected = selected;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Staff> getSearchName() {
		return searchName;
	}

	public void setSearchName(List<Staff> searchName) {
		this.searchName = searchName;
	}

	public String getSearchUser() {
		return searchUser;
	}

	public void setSearchUser(String searchUser) {
		this.searchUser = searchUser;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public SessionManager getSessionManager() {
		return sessionManager;
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

}
