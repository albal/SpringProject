package com.ikubInfo.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ikubInfo.pojo.Staff;

public class UserFilter implements Filter {
	private ServletContext context;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;
			String contextPath = req.getContextPath();
			String uri = req.getRequestURI();

			Staff staff = (Staff) req.getSession().getAttribute("user");

			if (staff != null && req.getSession() != null) {
				if (uri.contains("/admin/") || uri.contains("/shared/") && staff.getRole().getTitle().equals("admin")) {

					chain.doFilter(request, response);
				} else if (uri.contains("/manager/")
						|| uri.contains("/shared/") && staff.getRole().getTitle().equals("manager")) {

					chain.doFilter(request, response);
				} else if (uri.contains("/zookeeper/")
						|| uri.contains("/shared/") && staff.getRole().getTitle().equals("zookeeper")) {

					chain.doFilter(request, response);
				} else {
					res.sendRedirect(contextPath + "/login.xhtml");
				}
			} else {
				res.sendRedirect(contextPath + "/login.xhtml");
			}

		} catch (

		Throwable t) {
		}
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		this.context = fConfig.getServletContext();
		this.context.log("Initializing");

	}

	@Override
	public void destroy() {

	}

}
