package com.ikubInfo.converter;

import com.ikubInfo.entities.AnimalEntity;
import com.ikubInfo.pojo.Animal;

public class AnimalConverter {

	private static final AnimalCategoryConverter ANIMALCATEGORY_CONVERTER = new AnimalCategoryConverter();
	private static final EnclosureConverter ENCLOSURE_CONVERTER = new EnclosureConverter();

	public Animal doBackward(AnimalEntity animalEntity) {

		Animal animal = new Animal();
		if (animalEntity.getId() != null) {
			animal.setId(animalEntity.getId());
		}
		animal.setName(animalEntity.getName());
		animal.setScientificName(animalEntity.getScientificName());
		animal.setAge(animalEntity.getAge());
		animal.setGender(animalEntity.getGender());
		animal.setWeight(animalEntity.getWeight());
		animal.setArrivalDate(animalEntity.getArrivalDate());
		animal.setCreatedDate(animalEntity.getCreatedDate());
		animal.setValidity(animalEntity.getValidity());
		animal.setAnimalCategoryId(ANIMALCATEGORY_CONVERTER.doBackward(animalEntity.getCategoryId()));

		return animal;
	}

	public AnimalEntity doForward(Animal animal) {

		AnimalEntity animalEntity = new AnimalEntity();
		if (animal.getId() != null) {
			animalEntity.setId(animal.getId());
		}
		animalEntity.setName(animal.getName());
		animalEntity.setScientificName(animal.getScientificName());
		animalEntity.setAge(animal.getAge());
		animalEntity.setGender(animal.getGender());
		animalEntity.setWeight(animal.getWeight());
		animalEntity.setArrivalDate(animal.getArrivalDate());
		animalEntity.setCreatedDate(animal.getCreatedDate());
		if (animal.getValidity() != null) {
			animalEntity.setValidity(animal.getValidity());
		}
		if (animal.getAnimalCategoryId() != null) {
			animalEntity.setCategoryId(ANIMALCATEGORY_CONVERTER.doForward(animal.getAnimalCategoryId()));
		}
		if (animal.getEnclosureId() != null) {
			animalEntity.setEnclosureId(ENCLOSURE_CONVERTER.doForward(animal.getEnclosureId()));
		}
		return animalEntity;
	}

}
