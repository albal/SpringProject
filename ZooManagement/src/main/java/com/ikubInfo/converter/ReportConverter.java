package com.ikubInfo.converter;

import com.ikubInfo.entities.ReportEntity;
import com.ikubInfo.pojo.Report;

public class ReportConverter {

	private static final StaffTaskConverter STAFFTASK_CONVERTER = new StaffTaskConverter();

	public Report doBackward(ReportEntity reportEntity) {

		Report report = new Report();
		if (reportEntity.getId() != null) {
			report.setId(reportEntity.getId());
		}
		report.setMedicalObservation(report.getMedicalObservation());
		report.setDietObservation(reportEntity.getDietObservation());
		report.setComments(reportEntity.getComments());
		report.setValidity(reportEntity.getValidity());
		report.setCreatedDate(reportEntity.getCreatedDate());
		report.setStaffTaskId(STAFFTASK_CONVERTER.doBackward(reportEntity.getStaffTaskId()));

		return report;
	}

	public ReportEntity doForward(Report report) {

		ReportEntity reportEntity = new ReportEntity();
		if (report.getId() != null) {
			reportEntity.setId(report.getId());
		}
		reportEntity.setMedicalObservation(report.getMedicalObservation());
		reportEntity.setDietObservation(report.getDietObservation());
		reportEntity.setComments(report.getComments());
		if (report.getValidity() != null) {
			reportEntity.setValidity(report.getValidity());
		}
		reportEntity.setCreatedDate(report.getCreatedDate());
		reportEntity.setStaffTaskId(STAFFTASK_CONVERTER.doForward(report.getStaffTaskId()));

		return reportEntity;
	}

}
