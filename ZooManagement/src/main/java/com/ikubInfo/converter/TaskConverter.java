package com.ikubInfo.converter;

import com.ikubInfo.entities.TaskEntity;
import com.ikubInfo.pojo.Task;

public class TaskConverter{

	public Task doBackward(TaskEntity taskEntity) {

		Task task = new Task();
		if (taskEntity.getId() != null) {
			task.setId(taskEntity.getId());
		}
		task.setName(taskEntity.getName());
		task.setDescription(taskEntity.getDescription());
		task.setCreatedDate(taskEntity.getCreatedDate());
		task.setStatus(taskEntity.getStatus());
		task.setValidity(taskEntity.getValidity());

		return task;
	}

	public TaskEntity doForward(Task task) {

		TaskEntity taskEntity = new TaskEntity();
		if (task.getId() != null) {
			taskEntity.setId(task.getId());
		}
		taskEntity.setName(task.getName());
		taskEntity.setDescription(task.getDescription());
		taskEntity.setCreatedDate(task.getCreatedDate());
		if (task.getStatus() != null) {
			taskEntity.setStatus(task.getStatus());
		}
		if (task.getValidity() != null) {
			taskEntity.setValidity(task.getValidity());
		}
		return taskEntity;
	}

}
