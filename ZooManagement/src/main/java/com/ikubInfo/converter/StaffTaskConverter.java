package com.ikubInfo.converter;

import com.ikubInfo.entities.StaffTaskEntity;
import com.ikubInfo.pojo.StaffTask;

public class StaffTaskConverter {

	public StaffTaskEntity doForward(StaffTask a) {
		StaffTaskEntity staffTaskEntity = new StaffTaskEntity();
		if (a.getStaffId() != null) {
			staffTaskEntity.setStaffId(a.getStaffId());
		}

		if (a.getTaskId() != null) {
			staffTaskEntity.setTaskId(a.getTaskId());
		}
		return staffTaskEntity;
	}

	public StaffTask doBackward(StaffTaskEntity b) {
		StaffTask staffTask = new StaffTask();

		if (b.getStaffId() != null) {
			staffTask.setStaffId(b.getStaffId());
		}

		if (b.getTaskId() != null) {
			staffTask.setTaskId(b.getTaskId());
		}
		return staffTask;
	}

}
