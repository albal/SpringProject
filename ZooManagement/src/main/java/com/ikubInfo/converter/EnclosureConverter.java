package com.ikubInfo.converter;

import com.ikubInfo.entities.EnclosureEntity;
import com.ikubInfo.pojo.Enclosure;

public class EnclosureConverter {

	public Enclosure doBackward(EnclosureEntity enclosureEntity) {

		Enclosure enclosure = new Enclosure();

		enclosure.setId(enclosureEntity.getId());
		enclosure.setCapacity(enclosureEntity.getCapacity());
		enclosure.setType(enclosureEntity.getType());
		enclosure.setValidity(enclosureEntity.getValidity());

		return enclosure;
	}

	public EnclosureEntity doForward(Enclosure enclosure) {

		EnclosureEntity enclosureEntity = new EnclosureEntity();
		if (enclosure.getId() != null) {
			enclosureEntity.setId(enclosure.getId());
		}
		enclosureEntity.setCapacity(enclosure.getCapacity());
		enclosureEntity.setType(enclosure.getType());
		enclosureEntity.setValidity(enclosure.getValidity());

		return enclosureEntity;
	}

}
