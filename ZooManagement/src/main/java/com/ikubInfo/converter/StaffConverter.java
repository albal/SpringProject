package com.ikubInfo.converter;

import com.ikubInfo.entities.StaffEntity;
import com.ikubInfo.pojo.Staff;

public class StaffConverter {

	private static final RoleConverter ROLE_CONVERTER = new RoleConverter();

	public Staff doBackward(StaffEntity staffEntity) {
		Staff staff = new Staff();
		if (staffEntity.getId() != null) {
			staff.setId(staffEntity.getId());
		}
		staff.setFirstname(staffEntity.getFirstname());
		staff.setLastname(staffEntity.getLastname());
		staff.setUsername(staffEntity.getUsername());
		staff.setPassword(staffEntity.getPassword());
		staff.setAddress(staffEntity.getAddress());
		staff.setAge(staffEntity.getAge());
		staff.setHiredate(staffEntity.getHiredate());
		staff.setValidity(staffEntity.getValidity());
		if (staffEntity.getRole() != null) {
			staff.setRole(ROLE_CONVERTER.doBackward(staffEntity.getRole()));
		}
		return staff;
	}

	public StaffEntity doForward(Staff staff) {

		StaffEntity staffEntity = new StaffEntity();
		if (staff.getId() != null) {
			staffEntity.setId(staff.getId());
		}
		staffEntity.setFirstname(staff.getFirstname());
		staffEntity.setLastname(staff.getLastname());
		staffEntity.setUsername(staff.getUsername());
		staffEntity.setPassword(staff.getPassword());
		staffEntity.setAddress(staff.getAddress());
		staffEntity.setAge(staff.getAge());
		staffEntity.setHiredate(staff.getHiredate());
		if (staff.getValidity() != null) {
			staffEntity.setValidity(staff.getValidity());
		}

		if (staff.getRole() != null) {
			staffEntity.setRole(ROLE_CONVERTER.doForward(staff.getRole()));
		}
		return staffEntity;
	}

}
