package com.ikubInfo.converter;

import com.ikubInfo.entities.AnimalCategoryEntity;
import com.ikubInfo.pojo.AnimalCategory;

public class AnimalCategoryConverter {

	public AnimalCategory doBackward(AnimalCategoryEntity animalCategoryEntity) {

		AnimalCategory animalCategory = new AnimalCategory();
		if (animalCategoryEntity.getId() != null) {
			animalCategory.setId(animalCategoryEntity.getId());
		}
		animalCategory.setName(animalCategoryEntity.getName());
		animalCategory.setDescription(animalCategoryEntity.getDescription());
		animalCategory.setValidity(animalCategoryEntity.getValidity());

		return animalCategory;
	}

	public AnimalCategoryEntity doForward(AnimalCategory animalCategory) {

		AnimalCategoryEntity animalCategoryEntity = new AnimalCategoryEntity();
		if (animalCategory.getId() != null) {
			animalCategoryEntity.setId(animalCategory.getId());
		}
		animalCategoryEntity.setName(animalCategory.getName());
		animalCategoryEntity.setDescription(animalCategory.getDescription());
		if (animalCategory != null) {
			animalCategoryEntity.setValidity(animalCategory.getValidity());
		}
		return animalCategoryEntity;
	}

}
