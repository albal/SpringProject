package com.ikubInfo.converter;

import com.ikubInfo.entities.DailyDietEntity;
import com.ikubInfo.pojo.DailyDiet;

public class DailyDietConverter {

	private static final AnimalConverter ANIMAL_CONVERTER = new AnimalConverter();

	public DailyDiet doBackward(DailyDietEntity dailyDietEntity) {

		DailyDiet dailyDiet = new DailyDiet();

		if (dailyDietEntity.getId() != null) {
			dailyDiet.setId(dailyDietEntity.getId());
		}
		dailyDiet.setFoodType(dailyDietEntity.getFoodType());
		dailyDiet.setFoodPortion(dailyDietEntity.getFoodPortion());
		dailyDiet.setStartDate(dailyDietEntity.getStartDate());
		dailyDiet.setEndDate(dailyDietEntity.getEndDate());
		dailyDiet.setValidity(dailyDietEntity.getValidity());
		dailyDiet.setUnitMass(dailyDietEntity.getUnitMass());
		dailyDiet.setAnimal(ANIMAL_CONVERTER.doBackward(dailyDietEntity.getAnimal()));

		return dailyDiet;
	}

	public DailyDietEntity doForward(DailyDiet dailyDiet) {

		DailyDietEntity dailyDietEntity = new DailyDietEntity();

		if (dailyDiet.getId() != null) {
			dailyDietEntity.setId(dailyDiet.getId());
		}
		dailyDietEntity.setFoodType(dailyDiet.getFoodType());
		dailyDietEntity.setFoodPortion(dailyDiet.getFoodPortion());
		dailyDietEntity.setStartDate(dailyDiet.getStartDate());
		dailyDietEntity.setEndDate(dailyDiet.getEndDate());
		dailyDietEntity.setUnitMass(dailyDiet.getUnitMass());
		if (dailyDiet.getValidity() != null) {
			dailyDietEntity.setValidity(dailyDiet.getValidity());
		}
		dailyDietEntity.setAnimal(ANIMAL_CONVERTER.doForward(dailyDiet.getAnimal()));

		return dailyDietEntity;
	}

}
