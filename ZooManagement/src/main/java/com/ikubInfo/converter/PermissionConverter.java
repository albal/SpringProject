package com.ikubInfo.converter;

import com.ikubInfo.entities.PermissionEntity;
import com.ikubInfo.pojo.Permission;

public class PermissionConverter {

	
	public Permission doBackward(PermissionEntity permissionEntity) {

		Permission permission = new Permission();
		if (permissionEntity.getId() != null) {
			permission.setId(permissionEntity.getId());
		}
		permission.setName(permissionEntity.getName());
		permission.setValidity(permissionEntity.getValidity());

		return permission;
	}

	
	public PermissionEntity doForward(Permission permission) {

		PermissionEntity permissionEntity = new PermissionEntity();
		if (permission.getId() != null) {
			permissionEntity.setId(permission.getId());
		}
		permissionEntity.setName(permission.getName());
		if (permission.getValidity() != null) {
			permissionEntity.setValidity(permission.getValidity());
		}

		return permissionEntity;
	}

}
