package com.ikubInfo.converter;

import com.ikubInfo.entities.RoleEntity;
import com.ikubInfo.pojo.Role;

public class RoleConverter {

	public Role doBackward(RoleEntity roleEntity) {

		Role role = new Role();
		if (roleEntity.getId() != null) {
			role.setId(roleEntity.getId());
		}
		role.setTitle(roleEntity.getTitle());
		role.setValidity(roleEntity.getValidity());

		return role;
	}

	public RoleEntity doForward(Role role) {

		RoleEntity roleEntity = new RoleEntity();
		if (role.getId() != null) {
			roleEntity.setId(role.getId());
		}
		roleEntity.setTitle(role.getTitle());
		if (role.getValidity() != null) {
			roleEntity.setValidity(role.getValidity());
		}

		return roleEntity;
	}

}
