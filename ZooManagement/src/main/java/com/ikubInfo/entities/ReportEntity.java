package com.ikubInfo.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Report")
public class ReportEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	@Column(name = "medicalObservation")
	private String medicalObservation;

	@Column(name = "dietObservation")
	private String dietObservation;

	@Column(name = "comments")
	private String comments;

	@Column(name = "createdDate")
	private Date createdDate;

	@Column(name = "validity", insertable = false, nullable = false)
	private Integer validity = 1;

	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "staffId", referencedColumnName = "staffId"),
			@JoinColumn(name = "taskId", referencedColumnName = "taskId") })
	private StaffTaskEntity staffTaskId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMedicalObservation() {
		return medicalObservation;
	}

	public void setMedicalObservation(String medicalObservation) {
		this.medicalObservation = medicalObservation;
	}

	public String getDietObservation() {
		return dietObservation;
	}

	public void setDietObservation(String dietObservation) {
		this.dietObservation = dietObservation;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public StaffTaskEntity getStaffTaskId() {
		return staffTaskId;
	}

	public void setStaffTaskId(StaffTaskEntity staffTaskId) {
		this.staffTaskId = staffTaskId;
	}

}
