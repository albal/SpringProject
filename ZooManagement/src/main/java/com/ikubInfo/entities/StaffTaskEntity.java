package com.ikubInfo.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "Staff_Task")
@IdClass(StaffTaskIdClass.class)
public class StaffTaskEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Integer staffId;

	@Id
	private Integer taskId;

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

}
