package com.ikubInfo.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DailyDiet")
public class DailyDietEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	@Column(name = "foodType")
	private String foodType;

	@Column(name = "foodPortion")
	private Integer foodPortion;

	@Column(name = "startDate")
	private Date startDate;

	@Column(name = "endDate")
	private Date endDate;

	@Column(name = "unitMass")
	private String unitMass;

	@Column(name = "createdBy")
	private Integer createdBy;

	@Column(name = "validity", insertable = false, nullable = false)
	private Integer validity = 1;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "animalId", referencedColumnName = "id")
	private AnimalEntity animal;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFoodType() {
		return foodType;
	}

	public void setFoodType(String foodType) {
		this.foodType = foodType;
	}

	public Integer getFoodPortion() {
		return foodPortion;
	}

	public void setFoodPortion(Integer foodPortion) {
		this.foodPortion = foodPortion;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public AnimalEntity getAnimal() {
		return animal;
	}

	public void setAnimal(AnimalEntity animal) {
		this.animal = animal;
	}

	public String getUnitMass() {
		return unitMass;
	}

	public void setUnitMass(String unitMass) {
		this.unitMass = unitMass;
	}

}
