package com.ikubInfo.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Animal")
public class AnimalEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "scientificName")
	private String scientificName;

	@Column(name = "age")
	private Integer age;

	@Column(name = "weight")
	private double weight;

	@Column(name = "gender")
	private String gender;

	@Column(name = "arrivalDate")
	private Date arrivalDate;

	@Column(name = "createdDate")
	private Date createdDate;

	@Column(name = "validity", insertable = false, nullable = false)
	private Integer validity = 1;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoryId", referencedColumnName = "id")
	private AnimalCategoryEntity categoryId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "enclosureId", referencedColumnName = "id")
	private EnclosureEntity enclosureId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScientificName() {
		return scientificName;
	}

	public void setScientificName(String scientificName) {
		this.scientificName = scientificName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public AnimalCategoryEntity getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(AnimalCategoryEntity categoryId) {
		this.categoryId = categoryId;
	}

	public EnclosureEntity getEnclosureId() {
		return enclosureId;
	}

	public void setEnclosureId(EnclosureEntity enclosureId) {
		this.enclosureId = enclosureId;
	}

}
