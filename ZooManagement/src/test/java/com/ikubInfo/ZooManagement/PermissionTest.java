package com.ikubInfo.ZooManagement;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ikubInfo.pojo.Permission;
import com.ikubInfo.pojo.Staff;
import com.ikubInfo.serviceImpl.PermissionServiceImpl;
import com.ikubInfo.serviceImpl.StaffServiceImpl;

public class PermissionTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		@SuppressWarnings("resource")
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");

		StaffServiceImpl staffServiceImpl = (StaffServiceImpl) applicationContext.getBean("staffServiceImpl");

		PermissionServiceImpl permissionServiceImpl = (PermissionServiceImpl) applicationContext
				.getBean("permissionServiceImpl");

		Staff staf = staffServiceImpl.getById(2);
		System.out.println(staf.getUsername());

		List<Permission> permissions = permissionServiceImpl.getPermissionByRole(staf.getRole().getId());

		System.out.println(permissions.toString());

	}

}
