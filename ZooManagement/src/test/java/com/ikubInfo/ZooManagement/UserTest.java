package com.ikubInfo.ZooManagement;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ikubInfo.pojo.Permission;
import com.ikubInfo.pojo.Role;
import com.ikubInfo.pojo.Staff;
import com.ikubInfo.serviceImpl.PermissionServiceImpl;
import com.ikubInfo.serviceImpl.RoleServiceImpl;
import com.ikubInfo.serviceImpl.StaffServiceImpl;

public class UserTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		@SuppressWarnings("resource")
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");

		RoleServiceImpl roleService = (RoleServiceImpl) applicationContext.getBean("roleServiceImpl");

		PermissionServiceImpl permissionServiceImpl = (PermissionServiceImpl) applicationContext
				.getBean("permissionServiceImpl");

		StaffServiceImpl staffServiceImpl = (StaffServiceImpl) applicationContext.getBean("staffServiceImpl");

		/*
		 * staffServiceImpl.getAll().forEach(System.out::println);
		 * 
		 * 
		 * 
		 * 
		 * 
		 * Staff staff=new Staff(); staff.setUsername("lLl"); staff.setRoleId(task);
		 * 
		 * 
		 * staffServiceImpl.save(staff); System.out.println(staffServiceImpl.getAll());
		 */
		
		Role role=roleService.getById(1); 
		
		Staff staff=new  Staff();
		staff.setFirstname("aleks");
		staff.setLastname("beko");
		staff.setUsername("admin");
		staff.setPassword("admin1234");
		staff.setAddress("tirane");
		staff.setRole(role);
		staffServiceImpl.save(staff);

		

		/*
		 * List<Staff> staff = staffServiceImpl.getStaffByRole(); for (Staff s : staff)
		 * { System.out.println(s.getUsername()); }
		 * 
		 * List<Permission> per = permissionServiceImpl.getPermissionByRole(1); for
		 * (Permission p : per) { System.out.println(p.getName()); }
		 */

	}

}
