package com.ikubInfo.ZooManagement;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ikubInfo.pojo.Animal;
import com.ikubInfo.serviceImpl.AnimalServiceImpl;

public class AnimalTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		@SuppressWarnings("resource")
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");

		AnimalServiceImpl animalEntity = (AnimalServiceImpl) applicationContext.getBean("animalServiceImpl");

		/*
		 * List<Animal> animal = animalEntity.getAll(); for (Animal a : animal) {
		 * System.out.println(a.getName()); }
		 */

		Long a = animalEntity.animalsInEnclosure("LionCage");
		
		System.out.println(a);
	}

}
